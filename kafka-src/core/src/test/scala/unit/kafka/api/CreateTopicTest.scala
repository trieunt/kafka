package unit.kafka.api

import kafka.admin.CreateTopicCommand
import kafka.producer.KeyedMessage
import kafka.producer.Producer
import kafka.producer.ProducerConfig
import kafka.server.KafkaConfig
import kafka.utils.TestUtils
import kafka.utils.ZkUtils
import org.I0Itec.zkclient.ZkClient
import kafka.utils.ZKStringSerializer
import kafka.server.KafkaServer
import kafka.api.FetchRequestBuilder
import kafka.consumer.SimpleConsumer
import kafka.message.Message

class CreateTopicTest {

}

object CreateTopicTest extends App {
  var arguments = new Array[String](8)
  arguments(0) = "--zookeeper"
  arguments(1) = "10.254.53.33:2181"
  arguments(2) = "--replica"
  arguments(3) = "1"
  arguments(4) = "--partition"
  arguments(5) = "1"
  arguments(6) = "--topic"
  arguments(7) = "test-create-topic"
  CreateTopicCommand.main(arguments);
}

object ProducerSendMessage extends App {
  private val brokerId1 = 0
  private val brokerId2 = 1
  private var server1: KafkaServer = null
  private var server2: KafkaServer = null
  private var consumer1: SimpleConsumer = null
  private var consumer2: SimpleConsumer = null
  private val propertise1 = TestUtils.createBrokerConfig(brokerId1, 9092)
  private val config1 = new KafkaConfig(propertise1) {
    override val hostName = "localhost"
    override val numPartitions = 1
  }
  private val propertise2 = TestUtils.createBrokerConfig(brokerId2, 9092)
  private val config2 = new KafkaConfig(propertise2) {
    override val hostName = "localhost"
    override val numPartitions = 1
  }

  val props1 = new java.util.Properties()
  props1.put("serializer.class", "kafka.serializer.StringEncoder")
  props1.put("partitioner.class", "kafka.utils.StaticPartitioner")
  props1.put("metadata.broker.list", TestUtils.getBrokerListStrFromConfigs(Seq(config1, config2)))
  props1.put("request.required.acks", "2")
  props1.put("request.timeout.ms", "1000")

  val topic = "--test-create-topic"

  val producerConfig1 = new ProducerConfig(props1)
  val producer1 = new Producer[String, String](producerConfig1)
  // Available partition ids should be 0.
  producer1.send(new KeyedMessage[String, String](topic, "key1", "value1"))
  producer1.send(new KeyedMessage[String, String](topic, "key2", "value2"))

  var zkClient: ZkClient = null
  zkClient = new ZkClient("10.254.53.33:2181", 30000, 30000, ZKStringSerializer)
  //server1 = TestUtils.createServer(config1)
  //server2 = TestUtils.createServer(config2)
  consumer1 = new SimpleConsumer("10.254.53.33", 9092, 1000000, 64 * 1024, "")
  consumer2 = new SimpleConsumer("10.254.53.33", 9092, 100, 64 * 1024, "")
  val leaderOpt = ZkUtils.getLeaderForPartition(zkClient, topic, 0)
  val leader = leaderOpt.get
  val messageSet = if (leader == 0) {
    val response1 = consumer1.fetch(new FetchRequestBuilder().addFetch(topic, 0, 0, 10000).build())
    response1.messageSet(topic, 0).iterator.toBuffer
  } else {
    val response2 = consumer2.fetch(new FetchRequestBuilder().addFetch(topic, 0, 0, 10000).build())
    response2.messageSet(topic, 0).iterator.toBuffer
  }
  for (i <- 0 to messageSet.length - 1) {
    var bytes = new Array[Byte](messageSet(i).message.payload.limit());
    messageSet(i).message.payload.get(bytes);
    var value = new String(bytes, "UTF-8")
    println(value)
  }
//  val m = new Message(bytes = "test1".getBytes, key = "test".getBytes)
//  val m1 = messageSet(0).message.payload.limit()

  //  println(m1)
  //  val m2 = new Message(bytes = "test2".getBytes, key = "test".getBytes)
  //  val m3 = messageSet(1).message
  //  println(m2)
  //  println(m3)
  producer1.close()
}
